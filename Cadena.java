/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.cadena;

/**
 *
 * @author USUARIO
 */
import java.util.*;

public class Cadena {
    public static int numeroDePares(String[] words) {
        int cont = 0;
        
        for (int i = 0; i < words.length; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (esReversible(words[i], words[j])) {
                    cont = cont + 1 ;
                }
            }
        }
        
        return cont;
    }
    
    public static boolean esReversible(String palabra1, String palabra2) {
       return palabra1.equals(new StringBuilder(palabra2).reverse().toString());
    }
    
    public static void main(String[] args) {
        String[] words = {"ab", "ba", "cd", "fs", "zx", "sf"};
        int result = numeroDePares(words);
        System.out.println(result);}
    
}