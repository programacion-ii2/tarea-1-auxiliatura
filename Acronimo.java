/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.acronimo;

/**
 *
 * @author USUARIO
 */
public class Acronimo {

    public static boolean isAcronimo(String[] words, String s) {
        if (words.length != s.length()) {
            return false;
        }

        for (int i = 0; i < words.length; i++) {
            if (words[i].charAt(0) != s.charAt(i)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        String[] words = {"alice", "bob ", "charlie"};
        String s = "abc";
        boolean isAcronimo = isAcronimo(words,s); 

        System.out.println(isAcronimo);
    }
}
